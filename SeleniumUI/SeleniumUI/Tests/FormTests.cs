﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumUI.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SeleniumUI.Tests
{
    public class FormTests
    {
       
            IWebDriver driver;
            LoginForm loginForm;
            string userFirstName = "abcd";
            string userLastName = "abcd";
            string userPassword = "aaaAAA111";
            string message = "";
            string userEmail = "";
    [SetUp]
            public void Init()
            {
                driver = new ChromeDriver();
                driver.Manage().Cookies.DeleteAllCookies();
                driver.Manage().Window.Maximize();
                loginForm = new LoginForm(driver);
            }

            [Test]
            public void ValidateMessageIsDisplayedWhenUserSignUpsSuccesfully()
            {
                // Arrange
                
                userEmail = "aabbbbccccccc@email.com";


                // Act
                loginForm.GoToPage();
                loginForm.EnterFirstName(userFirstName);
                loginForm.EnterLastName(userLastName);
                loginForm.EnterEmail(userEmail);
                loginForm.EnterPassword(userPassword);
                loginForm.Dropbox();
                Thread.Sleep(2000);
                driver.FindElement(By.Id(" qa_loader-button")).Click();
                Thread.Sleep(2000);
                driver.FindElement(By.Id("qa_signup-participant")).Click();
                Thread.Sleep(2000);
            

            // Assert
            if (driver.FindElement(By.ClassName("profile-icon")) != null)
            {

                Console.WriteLine("Logged in successfully");
            }
            else
            {
                Console.WriteLine("Something is wrong");
            }
            Thread.Sleep(2000);
            
        }

        [Test]
            public void ValidateMessageIsDisplayedWhenUserSignUpsWithUsedEmail()
            {
            
            userEmail = "aabbbbc@email.com";
            message = "An user with this email is already registered";

            // Act
            loginForm.GoToPage();
            loginForm.EnterFirstName(userFirstName);
            loginForm.EnterLastName(userLastName);
            loginForm.EnterEmail(userEmail);
            loginForm.EnterPassword(userPassword);
            loginForm.Dropbox();
            Thread.Sleep(2000);
            driver.FindElement(By.Id(" qa_loader-button")).Click();
            Thread.Sleep(2000);
            // Assert
            Assert.IsTrue(loginForm.GetDisplayedMessage().Contains(message), "An user with this email is already registered'");
            
        }

        [Test]
        public void ValidateMessageCannotSignUPwithoutusernameWarning()
        {
            message = "This field can not be empty";

            // Act
            loginForm.GoToPage();
            loginForm.EnterFirstName(userFirstName);
            loginForm.EnterPassword(userPassword);
            Thread.Sleep(2000);
            loginForm.DeleteFirstName();

            
            Thread.Sleep(2000);
            
            // Assert
            Assert.IsTrue(loginForm.GetWarningMessage().Contains(message), "This field can not be empty'");

        }

        [Test]
        public void ValidateMessageInvalidPassword()
        {

            userPassword = "aaaaaa";
            message = "Password must contain at least 8 characters, 1 uppercase letter, 1 lowercase letter and 1 digit";

            // Act
            loginForm.GoToPage();
            loginForm.EnterFirstName(userFirstName);
            loginForm.EnterPassword(userPassword);
            loginForm.Dropbox();
            Thread.Sleep(2000);
            

            // Assert
            Assert.IsTrue(loginForm.GetWarningMessage1().Contains(message), "Password must contain at least 8 characters, 1 uppercase letter, 1 lowercase letter and 1 digit'");

        }

        [Test]
        public void ValidateMessageInvalidSecondPassword()
        {

            
            message = "Passwords must match";

            // Act
            loginForm.GoToPage();
            loginForm.EnterFirstName(userFirstName);
            loginForm.EnterFirstPassword(userPassword);
            userPassword = "ABC111aaa";
            loginForm.EnterSecondPassword(userPassword);
            loginForm.Dropbox();
            Thread.Sleep(2000);


            // Assert
            Assert.IsTrue(loginForm.GetWarningMessage1().Contains(message), "Passwords must match");

        }

        [TearDown]
            public void Cleanup()
            {
                driver.Quit();
            }
        }
}
