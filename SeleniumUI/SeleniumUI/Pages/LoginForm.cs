﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SeleniumUI.Pages
{
    public class LoginForm
    {
        private string url = "https://politrip.com/account/sign-up";

        private IWebDriver driver;

        public LoginForm(IWebDriver driver)
        {
            this.driver = driver;

            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "first-name")]
        private IWebElement firstnameTextBox;

        [FindsBy(How = How.Id, Using = "last-name")]
        private IWebElement lastnameTextBox;

        [FindsBy(How = How.Id, Using = "email")]
        private IWebElement emailTextBox;

        [FindsBy(How = How.Id, Using = "sign-up-password-input")]
        private IWebElement passwordTextBox;

        [FindsBy(How = How.Id, Using = "sign-up-confirm-password-input")]
        private IWebElement passwordConfirmTextBox;

        [FindsBy(How = How.Id, Using = "sign-up-heard-input")]
        private IWebElement dropBox;

        [FindsBy(How = How.Id, Using = " qa_loader-button")]
        private IWebElement showMessageButton;

        [FindsBy(How = How.Id, Using = "sign-up-error-div")]
        private IWebElement displayedMessage;

        [FindsBy(How = How.ClassName, Using = "form-input-hint")]
        private IWebElement displayedMessage1;

       

        public void GoToPage()
        {
            driver.Navigate().GoToUrl(url);
        }

        public void EnterFirstName(string message)
        {
            firstnameTextBox.SendKeys(message);
        }

        public void DeleteFirstName()
        {
            firstnameTextBox.SendKeys(Keys.Backspace);
            firstnameTextBox.SendKeys(Keys.Backspace);
            firstnameTextBox.SendKeys(Keys.Backspace);
            firstnameTextBox.SendKeys(Keys.Backspace);
            //firstnameTextBox.Clear(); did not work
        }

        public void EnterLastName(string message)
        {
            lastnameTextBox.SendKeys(message);
        }

        public void EnterEmail(string message)
        {
            emailTextBox.SendKeys(message);
        }
        public void EnterPassword(string message)
        {
            passwordTextBox.SendKeys(message);
            passwordConfirmTextBox.SendKeys(message);
        }

        public void EnterFirstPassword(string message)
        {
            passwordTextBox.SendKeys(message);
        }

        public void EnterSecondPassword(string message)
        {
            passwordConfirmTextBox.SendKeys(message);
        }

            public void Dropbox()
        {
            //create select element object 
            var selectElement = new SelectElement(dropBox);
            //select by value
            selectElement.SelectByValue("socialNetworks");
        }
        public LoginForm ClickShowMessage()
        {
            showMessageButton.Click();
            return this;
        }

        public string GetDisplayedMessage()
        {
            return displayedMessage.Text;
        }

        public string GetWarningMessage()
        {
            return displayedMessage1.Text;
        }

        public string GetWarningMessage1()
        {
            return displayedMessage1.Text;
        }
    }
}
