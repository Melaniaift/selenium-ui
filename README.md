Problem description
Create a test suite using Selenium for the sign-up flow of the Politrip application that can be
found at https://politrip.com/account/sign-up.


Additional requirements
- The programming language and the test framework can be chosen by the candidate.
- The number of the tests in the test suite is decided by the candidate.

